package com.example.homecredithackathon.ui.scan

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.*
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.budiyev.android.codescanner.*
import com.example.homecredithackathon.R
import com.example.homecredithackathon.databinding.ScannerLayoutBinding
import com.example.homecredithackathon.utils.start
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class ScanFragment : Fragment(R.layout.scanner_layout) {

    private var codeScanner: CodeScanner? = null
    private var _scannerLayoutBinding: ScannerLayoutBinding? = null
    private val scannerLayoutBinding get() = _scannerLayoutBinding!!

    private val scanViewModel: ScanViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(),
                listOf(android.Manifest.permission.CAMERA).toTypedArray(), 50);
        }

        _scannerLayoutBinding = ScannerLayoutBinding.bind(view)

        codeScanner = CodeScanner(requireContext(), scannerLayoutBinding.scannerView)

        // Parameters (default values)
        codeScanner?.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner?.formats = CodeScanner.ALL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner?.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner?.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner?.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner?.isFlashEnabled = false // Whether to enable flash or not
        codeScanner?.startPreview()
        codeScanner?.apply {
            decodeCallback = DecodeCallback {
                if (it.text.equals("аниме")) scanViewModel.getProductByQr()
            }
            errorCallback = ErrorCallback {
                lifecycleScope.launch(Dispatchers.Main) {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
            }
        }

        scanViewModel.navigationFlow.onEach {
            if (it) {
                codeScanner?.stopPreview()
                findNavController().navigate(R.id.to_productFragment)
            }
        }.start(lifecycleScope = lifecycleScope, lifecycle)
    }

    override fun onResume() {
        super.onResume()
        codeScanner?.startPreview()
    }

    override fun onPause() {
        codeScanner?.releaseResources()
        super.onPause()
    }

    override fun onDestroyView() {
        _scannerLayoutBinding = null
        super.onDestroyView()
    }

}