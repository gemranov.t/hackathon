package com.example.homecredithackathon.ui.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.homecredithackathon.R
import com.example.homecredithackathon.databinding.HomeFragmentBinding

class HomeFragment: Fragment(R.layout.home_fragment) {

    private var _homeFragmentBinding: HomeFragmentBinding? = null
    private val homeFragmentBinding get() = _homeFragmentBinding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _homeFragmentBinding = HomeFragmentBinding.bind(view)

        homeFragmentBinding.homeCard.setOnClickListener {
            findNavController().navigate(R.id.to_scanFragment)
        }

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        _homeFragmentBinding = null
        super.onDestroyView()
    }

}