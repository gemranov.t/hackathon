package com.example.homecredithackathon.data

import com.example.homecredithackathon.Api
import com.example.homecredithackathon.base.*

class Repository(private val api: Api): CoroutineCaller by ApiCaller {

    suspend fun getProductByQr() = apiCall {
        api.getProducts()
    }
}