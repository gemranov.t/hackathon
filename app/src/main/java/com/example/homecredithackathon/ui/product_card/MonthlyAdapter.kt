package com.example.homecredithackathon.ui.product_card

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.homecredithackathon.BaseAdapter
import com.example.homecredithackathon.BaseViewHolder
import com.example.homecredithackathon.R
import com.example.homecredithackathon.data.Month
import kotlinx.android.synthetic.main.monthly_item.view.*

class MonthlyAdapter : BaseAdapter<Month, MonthViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MonthViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.monthly_item, parent, false)
        return MonthViewHolder(view)
    }

    override fun onBindViewHolder(holder: MonthViewHolder, position: Int) {
        changeItemViewColor(items[position], holder.itemView.month_tv)
    }
}

fun changeItemViewColor(item: Month, textView: TextView){
    textView.apply {
        if (item.isSelected) {
            setBackgroundColor(context.resources.getColor(R.color.red, null))
            setTextColor(context.resources.getColor(R.color.red, null))
            item.isSelected = false
        } else {
            setBackgroundColor(context.resources.getColor(R.color.grey2, null))
            setTextColor(context.resources.getColor(R.color.grey2, null))
            item.isSelected = true
        }
    }
}


class MonthViewHolder(view: View) : BaseViewHolder<Month>(view) {
    override fun bind(item: Month) {
        itemView.month_tv.text = item.month
        itemView.month_tv.setOnClickListener {
            changeItemViewColor(item, itemView.month_tv)
        }
    }
}