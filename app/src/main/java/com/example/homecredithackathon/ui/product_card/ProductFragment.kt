package com.example.homecredithackathon.ui.product_card

import android.graphics.drawable.Drawable
import androidx.fragment.app.Fragment
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.*
import androidx.viewpager2.widget.ViewPager2
import com.example.homecredithackathon.R
import com.example.homecredithackathon.data.Month
import com.example.homecredithackathon.databinding.ProductLayoutBinding
import com.example.homecredithackathon.ui.apply_loan.RecommendedAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProductFragment: Fragment(R.layout.product_layout) {

    private val productViewModel: ProductCardViewModel by viewModel()

    private var _productFragmentBinding: ProductLayoutBinding? = null
    private val productFragmentBinding get() = _productFragmentBinding!!

    private val dots = arrayListOf<TextView>()

    private val months by lazy {
        mutableListOf(
            Month("3"),
            Month("6"),
            Month("9"),
            Month("12"),
            Month("24"),
            Month("36")
        )
    }

    private val monthlyAdapter by lazy(LazyThreadSafetyMode.NONE) { MonthlyAdapter() }
    private val sliderAdapter by lazy(LazyThreadSafetyMode.NONE) { SliderAdapter() }
    private val recommendedAdapter by lazy(LazyThreadSafetyMode.NONE) { RecommendedAdapter() }

    companion object {
        const val SELECTED = R.drawable.pager_dot_selected
        const val NOT_SELECTED = R.drawable.pager_dot_not_selected
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        dots.addAll(listOf(TextView(requireContext()), TextView(requireContext()), TextView(requireContext())))
        _productFragmentBinding = ProductLayoutBinding.bind(view)
        productFragmentBinding.monthlyRecycler.apply {
            adapter = monthlyAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        }

        initDots()

        productFragmentBinding.productImages.adapter = sliderAdapter

        sliderAdapter.items = mutableListOf(
            "http://159.223.22.94/media/apple-iphone-13-01.jpg",
            "http://159.223.22.94/media/apple-iphone-13-2.jpg",
            "http://159.223.22.94/media/apple-iphone-13-3.jpg"
        )

        monthlyAdapter.items = months

        productFragmentBinding.productImages.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                selectDot(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })

        productFragmentBinding.recommendedRv.apply {
            adapter = recommendedAdapter
            layoutManager = GridLayoutManager(requireContext(), 2)
        }

        productFragmentBinding.compareProductBtn.setOnClickListener {
            findNavController().navigate(R.id.to_compareFragment)
        }

        productFragmentBinding.buyProductBtn.setOnClickListener {
            findNavController().navigate(R.id.to_applyLoanFragment)
        }

        super.onViewCreated(view, savedInstanceState)
    }

    private fun selectDot(idx: Int, ) {
        for (i in 0 until 2) {
            val drawableId: Int =
                if (i == idx) SELECTED else NOT_SELECTED
            val drawable: Drawable = ResourcesCompat.getDrawable(resources, drawableId, null)!!
            dots[i].background = drawable
        }
    }

    private fun initDots(){
        for (i in 0 until 2) {
            val drawableId: Int =
                if (i == 0) SELECTED else NOT_SELECTED
            val drawable: Drawable = ResourcesCompat.getDrawable(resources, drawableId, null)!!
            dots[i].background = drawable
            productFragmentBinding.dotsContainer.addView(dots[i])
        }
    }

    override fun onDestroyView() {
        _productFragmentBinding = null
        super.onDestroyView()
    }

}