package com.example.homecredithackathon.ui.product_card

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coil.load
import com.example.homecredithackathon.BaseAdapter
import com.example.homecredithackathon.BaseViewHolder
import com.example.homecredithackathon.R
import kotlinx.android.synthetic.main.slider_item.view.*

class SliderAdapter: BaseAdapter<String, SliderViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SliderViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.slider_item, parent, false)
        return SliderViewHolder(view)
    }
}

class SliderViewHolder(view: View): BaseViewHolder<String>(view){
    override fun bind(item: String) {
        itemView.slider_image.load(item)
    }

}