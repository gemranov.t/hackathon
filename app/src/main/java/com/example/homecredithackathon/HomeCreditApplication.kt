package com.example.homecredithackathon

import android.app.Application
import com.example.homecredithackathon.module.networkModule
import com.example.homecredithackathon.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class HomeCreditApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@HomeCreditApplication)
            modules(viewModelModule, networkModule)
        }
    }
}