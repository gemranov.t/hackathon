package com.example.homecredithackathon.data

data class Month(val month: String, var isSelected: Boolean = false)