package com.example.homecredithackathon.base

import com.google.gson.JsonSyntaxException
import kotlinx.coroutines.coroutineScope
import retrofit2.HttpException
import java.net.*

interface CoroutineCaller {
    suspend fun <T> apiCall(result: suspend () -> T): RequestResult<T>
}

object ApiCaller: CoroutineCaller {

    override suspend fun <T> apiCall(result: suspend () -> T): RequestResult<T> = try {
        coroutineScope {
            RequestResult.Success(
                result.invoke()
            )
        }
    } catch (e: Exception) {
        handleException(e)
    }

    private fun <T> handleException(e: Exception): RequestResult<T> = when (e) {
        is JsonSyntaxException -> {
            RequestResult.Failure("IdResourceString(R.string.request_json_error)")
        }
        is ConnectException -> {
            RequestResult.Failure("IdResourceString(R.string.request_connection_error)")
        }
        is SocketTimeoutException -> {
            RequestResult.Failure("IdResourceString(R.string.request_timeout)")
        }
        is HttpException -> when (e.code()) {
            HttpURLConnection.HTTP_NOT_FOUND,
            HttpURLConnection.HTTP_BAD_REQUEST,
            HttpURLConnection.HTTP_FORBIDDEN -> {
                RequestResult.Failure(
                    e.response()?.errorBody()?.string(),
                    e.code()
                )
            }
            HttpURLConnection.HTTP_INTERNAL_ERROR -> {
                RequestResult.Failure(
                    "IdResourceString(R.string.request_http_error_500)",
                    e.code()
                )
            }
            else -> RequestResult.Failure(
                "else",
                e.code()
            )
        }
        else -> RequestResult.Failure(
            "else",
            1
        )
    }
}