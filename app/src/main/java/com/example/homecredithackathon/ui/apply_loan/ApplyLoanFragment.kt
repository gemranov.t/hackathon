package com.example.homecredithackathon.ui.apply_loan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.homecredithackathon.R
import com.example.homecredithackathon.databinding.ApplyLoanLayoutBinding

class ApplyLoanFragment: Fragment(R.layout.apply_loan_layout) {
    private val recommendedAdapter by lazy { RecommendedAdapter() }

    private var _applyLoanLayoutBinding: ApplyLoanLayoutBinding? = null
    private val applyLoanLayoutBinding get() = _applyLoanLayoutBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _applyLoanLayoutBinding = ApplyLoanLayoutBinding.bind(view)
        applyLoanLayoutBinding?.recommendedRv?.apply {
            adapter = recommendedAdapter
            layoutManager = GridLayoutManager(requireContext(), 2)
        }
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        _applyLoanLayoutBinding = null
        super.onDestroyView()
    }

}