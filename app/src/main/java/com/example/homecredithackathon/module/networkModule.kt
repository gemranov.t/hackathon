package com.example.homecredithackathon.module

import com.example.homecredithackathon.Api
import com.example.homecredithackathon.data.Repository
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

const val NETWORK_TIMEOUT_IN_SECONDS = 30L
const val BASE_URL = "http://159.223.22.94/"
const val TOKEN = "00beb45100f6d0fff6fbf6dd6444aa78d40d5f48"

val networkModule = module {

    single { provideRetrofit() }
    single { GsonConverterFactory.create(get()) }
    single { provideHttpClient(get()) }
    single { get<Retrofit>().create(Api::class.java) }

    factory { Repository(api = get()) }
}

fun provideHttpClient(httpClientBuilder: OkHttpClient.Builder): OkHttpClient {
    return httpClientBuilder
        .addInterceptor { chain ->
            chain.request().newBuilder()
                .addHeader("Authorization", "Token $TOKEN")
                .build()
                .let(chain::proceed)
        }
        .callTimeout(NETWORK_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
        .readTimeout(NETWORK_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
        .connectTimeout(NETWORK_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
        .build()
}

fun provideRetrofit(): Retrofit {

    val httpClientBuilder = OkHttpClient.Builder().apply {
        addInterceptor(HttpLoggingInterceptor().apply {
            setLevel(HttpLoggingInterceptor.Level.BODY)

        })

    }
    return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(provideHttpClient(httpClientBuilder))
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}