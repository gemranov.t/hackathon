package com.example.homecredithackathon.ui.compare

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.homecredithackathon.R
import org.koin.androidx.viewmodel.ext.android.viewModel

class CompareFragment: Fragment(R.layout.compare_layout) {
    private val compareViewModel: CompareViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

}