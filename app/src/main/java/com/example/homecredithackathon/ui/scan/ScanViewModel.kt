package com.example.homecredithackathon.ui.scan

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.homecredithackathon.base.RequestResult
import com.example.homecredithackathon.data.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch

class ScanViewModel(private val repository: Repository): ViewModel() {

    val navigationFlow = MutableSharedFlow<Boolean>()

    fun getProductByQr(){
        viewModelScope.launch(Dispatchers.IO) {
            when(repository.getProductByQr()){
                is RequestResult.Success -> {
                    navigationFlow.emit(true)
                }
                else ->  navigationFlow.emit(true)
            }
        }
    }
}