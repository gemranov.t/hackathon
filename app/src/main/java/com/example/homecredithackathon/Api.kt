package com.example.homecredithackathon

import com.example.homecredithackathon.data.ResponseProducts
import retrofit2.http.GET

interface Api {
    @GET("products/")
    suspend fun getProducts(): ResponseProducts
}