package com.example.homecredithackathon.ui.scoring

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.onEach

class ScoringViewModel: ViewModel() {

    val timer = (0..100)
        .asSequence()
        .asFlow()
        .onEach { delay(100) }
}