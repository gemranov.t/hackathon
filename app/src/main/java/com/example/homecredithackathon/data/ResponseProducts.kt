package com.example.homecredithackathon.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseProducts(

	@field:SerializedName("ResponseProducts")
	val responseProducts: List<ResponseProductsItem?>? = null
) : Parcelable

@Parcelize
data class Shop(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
) : Parcelable

@Parcelize
data class Memory(
	@field:SerializedName("Оперативная память")
	val ram: String? = null
): Parcelable

@Parcelize
data class GeneralCharacter(
	@field:SerializedName("Серия")
	val series : String? = null
): Parcelable

@Parcelize
data class Specification(

	@field:SerializedName("Основные характеристики")
	val mainCharacter: GeneralCharacter? = null,

	@field:SerializedName("Память")
	val memory : Memory? = null
) : Parcelable

@Parcelize
data class ImagesItem(

	@field:SerializedName("file")
	val file: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
) : Parcelable

@Parcelize
data class ResponseProductsItem(

	@field:SerializedName("images")
	val images: List<ImagesItem>? = null,

	@field:SerializedName("shop")
	val shop: Shop? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("purchase_url")
	val purchaseUrl: String? = null,

	@field:SerializedName("specification")
	val specification: Specification? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("qr_code_url")
	val qrCodeUrl: String? = null
) : Parcelable