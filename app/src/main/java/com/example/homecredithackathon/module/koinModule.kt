package com.example.homecredithackathon.module

import com.example.homecredithackathon.ui.compare.CompareViewModel
import com.example.homecredithackathon.ui.product_card.ProductCardViewModel
import com.example.homecredithackathon.ui.scan.ScanViewModel
import com.example.homecredithackathon.ui.scoring.ScoringViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule  = module {
    viewModel { ScanViewModel(repository = get()) }
    viewModel { ProductCardViewModel() }
    viewModel { ScoringViewModel() }
    viewModel { CompareViewModel() }
}