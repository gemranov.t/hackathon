package com.example.homecredithackathon.base

sealed class RequestResult<out T : Any?> {
    data class Success<out T : Any?>(val result: T) : RequestResult<T>()
    data class Failure(
        val error: String?,
        val code: Int = 0
    ) : RequestResult<Nothing>()
    data class Error(val exception: Exception) : RequestResult<Nothing>()
}