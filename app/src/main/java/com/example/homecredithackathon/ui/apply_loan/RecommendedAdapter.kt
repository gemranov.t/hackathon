package com.example.homecredithackathon.ui.apply_loan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.homecredithackathon.BaseAdapter
import com.example.homecredithackathon.BaseViewHolder
import com.example.homecredithackathon.R
import com.example.homecredithackathon.data.Month
import com.example.homecredithackathon.ui.product_card.MonthViewHolder

class RecommendedAdapter : BaseAdapter<Month, MonthViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MonthViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.monthly_item, parent, false)
        return MonthViewHolder(view)
    }
}

class RecommendedViewHolder(view: View): BaseViewHolder<String>(view){
    override fun bind(item: String) {
        TODO("Not yet implemented")
    }

}