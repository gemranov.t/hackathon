package com.example.homecredithackathon.ui.scoring

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.homecredithackathon.R
import com.example.homecredithackathon.databinding.ScoringLayoutBinding
import com.example.homecredithackathon.utils.start
import kotlinx.coroutines.flow.onEach

class ScoringFragment: Fragment(R.layout.scoring_layout) {

    private val scoringViewModel: ScoringViewModel by viewModels()

    private var _scoringLayoutBinding: ScoringLayoutBinding? = null
    private val scoringLayoutBinding get() = _scoringLayoutBinding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _scoringLayoutBinding = ScoringLayoutBinding.bind(view)

        scoringViewModel.timer.onEach {
            updateProgressBar(it)
        }.start(lifecycle = lifecycle, lifecycleScope = lifecycleScope)

        super.onViewCreated(view, savedInstanceState)
    }

    private fun updateProgressBar(percentage: Int = 0) {
        scoringLayoutBinding.scoringPb.progress = percentage
        scoringLayoutBinding.percentageTv.text = "$percentage%"

        if (percentage == 100){
            scoringLayoutBinding.apply {
                scoringPb.visibility = View.GONE
                percentageTv.visibility = View.GONE
                successScoring.visibility = View.VISIBLE
                scroingTv2.visibility = View.GONE
                contineScoringBtn.visibility = View.VISIBLE
                scoringTv.text = "Заявка на кредит одобрена"
            }
        }
    }

    override fun onDestroyView() {
        _scoringLayoutBinding = null
        super.onDestroyView()
    }

}